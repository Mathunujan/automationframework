package testbases;

import com.sun.istack.internal.logging.Logger;
import org.testng.annotations.DataProvider;

public class LoginTestData extends XlData {

    private static final Logger LOGGER = Logger.getLogger(LoginTestData.class);
    public LoginTestData () {
        super("src\\main\\resources\\Exl\\testData.xlsx");
    }
    @DataProvider(name = "LoginTestData")
    public Object[][] LoginTestData() {
        int rows = getRowCount("Sheet1");
        int col = getColumnCount("Sheet1");
        LOGGER.info("row = "+ rows + " columns = "+ col);
        Object[][] data = new Object[rows][col];
        for (int i = 1; i <= rows; i++) {
            for (int j = 0; j < col; j++) {
                data[i - 1][j] = getData("Sheet1", i, j);
            }
        }
        return data;
    }
}
