package pages;

import com.sun.istack.internal.logging.Logger;
import org.openqa.selenium.By;
import utils.PageBase;

public class LoginPage extends PageBase {
    private static final Logger LOGGER = Logger.getLogger(HomePage.class);

    private static By userNameWeb= By.id("txtUsername");
    private static By userPassword= By.id("txtPassword");
    private static By click= By.id("btnLogin");



    public static void setUserName(String userName) {
        getDriver().findElement(userNameWeb).sendKeys(userName);
    }
    public static void setPassword(String password) {
        getDriver().findElement(userPassword).sendKeys(password);
    }
    public static void clickLogin() {
        getDriver().findElement(click).click();
    }
//    public static boolean isLoginAlertDisplay() {
//        return getDriver().findElement(alert).isDisplayed();
//    }
//    public static String getLoginAlert() {
//        return  getDriver().findElement(alert).getText();
//    }
}
