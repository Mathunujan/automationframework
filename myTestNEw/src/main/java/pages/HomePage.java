package pages;

import com.sun.istack.internal.logging.Logger;
import org.openqa.selenium.By;
import utils.PageBase;



public class HomePage extends PageBase {

  //  public SoftAssert softAssert;
    private static final Logger LOGGER = Logger.getLogger(HomePage.class);


    private static By A_bTesting= By.xpath("//*[@href=\"/abtest\"]");
    private static By A_bTestControl= By.xpath("//*[@class=\"no-js\"]");

    public static void ClickA_Blink(){
        getDriver().findElement(A_bTesting).click();
    }

    public static boolean A_B_isDisplayed(){
        return getDriver().findElement(A_bTestControl).isDisplayed();
    }

    public static By elementReturn(){
        return A_bTesting;

    }



}
