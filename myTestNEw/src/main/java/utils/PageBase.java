package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class PageBase {
    public static WebDriver driver;
    private static String baseUrl="https://opensource-demo.orangehrmlive.com/";
    private static String driverPath="src"+ File.separator+"main"+ File.separator+"resources"+ File.separator+"drivers"+File.separator;
   // protected static String uploadFilepath = System.getProperty("user.dir")+File.separator+"src"+File.separator+"test"+File.separator+"resources"+File.separator+"fileUpload";
    private static String osType=System.getProperty("os.Type",Constants.WINDOWS);
    private static String driverType=System.getProperty("browser.type",Constants.CHROME);


    public static void initiateDriver() throws MalformedURLException {
        if (Constants.CHROME.equals(driverType)) {
            if (osType.equals(Constants.UBUNTU)) {
                System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver");
            } else
                System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
            driver = new ChromeDriver();
        } else if (Constants.FIREFOX.equals(driverType)) {
            if (osType.equals(Constants.UBUNTU))
                System.setProperty("webdriver.gecko.driver", driverPath + "geckodriver");
            else System.setProperty("webdriver.gecko.driver", driverPath + "geckodriver.exe");
            driver = new FirefoxDriver();
        }
        getDriver().manage().window().maximize();
        getDriver().get(baseUrl);
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static void closeDriver(){
        getDriver().quit();
    }

    public static void implicitWait(int seconds){
        getDriver().manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);


    }

    public static void waitTillClickable(By element, int seconds){
        WebDriverWait wait =new WebDriverWait(getDriver(),seconds);
        wait.until(ExpectedConditions.elementToBeClickable(element));

    }





}
